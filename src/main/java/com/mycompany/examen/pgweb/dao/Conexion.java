/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examen.pgweb.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author USUARIO
 */
public class Conexion {
    private static Conexion con;
    private EntityManagerFactory bd; 
 
    public Conexion() {
        this.bd=Persistence
                .createEntityManagerFactory("com.mycompany_examen-pgweb_war_1.0-SNAPSHOTPU");  
    }
     
    public static Conexion getConexion()
    {
        if(con==null)
        {
            con=new Conexion();
        }
    return con;
    }
 
    public EntityManagerFactory getDatabase() {
        return bd;
    }
}
