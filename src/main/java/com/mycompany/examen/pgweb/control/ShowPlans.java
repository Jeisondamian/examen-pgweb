/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examen.pgweb.control;

import com.mycompany.examen.pgweb.dao.Conexion;
import com.mycompany.examen.pgweb.dao.PlanMovilJpaController;
import com.mycompany.examen.pgweb.dao.ServicioXplanJpaController;
import com.mycompany.examen.pgweb.dao.ServiciosAdicionalesJpaController;
import com.mycompany.examen.pgweb.dto.PlanMovil;
import com.mycompany.examen.pgweb.dto.ServicioXplan;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USUARIO
 */
public class ShowPlans extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServicioXplanJpaController servicioXplanControl =
                new ServicioXplanJpaController(Conexion.getConexion().getDatabase());
        PlanMovilJpaController planMovilControl = 
                new PlanMovilJpaController(Conexion.getConexion().getDatabase());
        List<ServicioXplan> serviciosXplanes = servicioXplanControl.findServicioXplanEntities();
        List<PlanMovil> planesMoviles = planMovilControl.findPlanMovilEntities();
        //contruyendo la tabla
        String table = "";
        String head = "<thead><tr class='table-primary'><th>ID</th><th>NOMBRE</th><th>PRECIO</th></tr></thead>" +
                "<tbody>@@</tbody>";
        String row = "<tr>@@</tr>";
        String data = "<td>@@</td>";
        String rowContent = "";
        String tableContent = "";
        for(PlanMovil plan:planesMoviles){
            int cant = 2;
            rowContent += "<td rowspan='%%'>@@</td>".replace("@@", plan.getCodigoPlan()+"");
            rowContent += data.replace("@@", plan.getNombrePlan());
            rowContent += data.replace("@@", "$"+plan.getPrecio());
            rowContent = row.replace("@@", rowContent);
            rowContent += row.replace("@@", "<td colspan='2' class='text-center'>Adicionales</td>");
            tableContent += rowContent;
            //tableContent = 
            for(ServicioXplan sp:serviciosXplanes){
                if(sp.getPlanMovil().getCodigoPlan() == plan.getCodigoPlan()){
                    rowContent = data.replace("@@", sp.getServiciosAdicionales().getNombreServicio());
                    rowContent += data.replace("@@", "$0");
                    rowContent = row.replace("@@", rowContent);
                    tableContent += rowContent;
                    cant++;
                }
            }
            table += head.replace("@@", tableContent).replace("%%", cant+"");
            rowContent = "";
            tableContent = "";
        }
        request.setAttribute("table", table);
        request.getRequestDispatcher("./jsp/todoInfoPlanes.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
