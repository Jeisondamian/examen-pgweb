/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examen.pgweb.control;

import com.mycompany.examen.pgweb.dao.Conexion;
import com.mycompany.examen.pgweb.dao.ServiciosAdicionalesJpaController;
import com.mycompany.examen.pgweb.dao.SmartphoneJpaController;
import com.mycompany.examen.pgweb.dto.ServiciosAdicionales;
import com.mycompany.examen.pgweb.dto.Smartphone;
import com.mycompany.examen.pgweb.negocio.Cotizacion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USUARIO
 */
public class ContinueQuoting extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //obteniendo el codigo del plan enviado a traves del formulario
        String[] services = request.getParameterValues("service");
        String txtServices = "";

        //iniciando controllers
        ServiciosAdicionalesJpaController servicioAdicionalControl
                = new ServiciosAdicionalesJpaController(Conexion.getConexion().getDatabase());
        ServiciosAdicionales servicio = new ServiciosAdicionales();
        if (services != null) {
            for (int i = 0; i < services.length; i++) {
                servicio = servicioAdicionalControl
                        .findServiciosAdicionales(Integer.parseInt(services[i]));
                if (i == services.length - 1) {
                    txtServices += servicio.getNombreServicio() + ".";
                } else {
                    txtServices += servicio.getNombreServicio() + ", ";
                }
                Cotizacion.setServicios(servicio);
            }
        }
        SmartphoneJpaController smartphoneControl
                = new SmartphoneJpaController(Conexion.getConexion().getDatabase());

        //creando etiquetas html
        String check = "<label for='@@'><input type='checkbox' name='smartphone' id='@@' value='$$'>@@</label>";
        String checkgroup = "";
        List<Smartphone> smartphones = smartphoneControl.findSmartphoneEntities();
        for (Smartphone s : smartphones) {
            checkgroup += check.replaceAll("@@", s.getNombre()).replace("$$", s.getId() + "");
        }

        //envio de objetos y redireccionamiento
        request.setAttribute("services", txtServices);
        request.setAttribute("smartphones", checkgroup);
        request.getRequestDispatcher("./jsp/smartphone.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
