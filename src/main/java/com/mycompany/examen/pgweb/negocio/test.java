/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examen.pgweb.negocio;

import com.mycompany.examen.pgweb.dao.Conexion;
import com.mycompany.examen.pgweb.dao.PlanMovilJpaController;
import com.mycompany.examen.pgweb.dto.PlanMovil;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Conexion con = new Conexion();
        PlanMovilJpaController plan = new PlanMovilJpaController(con.getDatabase());
        List<PlanMovil> planes = plan.findPlanMovilEntities();
        for (PlanMovil p : planes) {
            System.out.println(p.getCodigoPlan() + "," + p.getNombrePlan() + "," + p.getPrecio());
        }

    }
    
}
