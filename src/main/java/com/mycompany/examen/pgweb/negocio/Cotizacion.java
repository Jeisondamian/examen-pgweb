/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examen.pgweb.negocio;

import com.mycompany.examen.pgweb.dto.PlanMovil;
import com.mycompany.examen.pgweb.dto.ServiciosAdicionales;
import com.mycompany.examen.pgweb.dto.Smartphone;

/**
 *
 * @author USUARIO
 */
public class Cotizacion {

    private static String nombre;
    private static PlanMovil plan;
    private static ServiciosAdicionales[] servicios = new ServiciosAdicionales[99];
    private static Smartphone[] smartphones = new Smartphone[99];

    public static String getNombre() {
        return nombre;
    }

    public static void setNombre(String nombre) {
        Cotizacion.nombre = nombre;
    }

    public static PlanMovil getPlan() {
        return plan;
    }

    public static void setPlan(PlanMovil plan) {
        Cotizacion.plan = plan;
    }

    public static ServiciosAdicionales[] getServicios() {
        return servicios;
    }

    public static void setServicios(ServiciosAdicionales servicio) {
        if (!existeServicio(servicio)) {
            for (int i = 0; i < Cotizacion.servicios.length; i++) {
                if (Cotizacion.servicios[i] == null) {
                    Cotizacion.servicios[i] = servicio;
                    break;
                }
            }
        }

    }

    private static boolean existeServicio(ServiciosAdicionales nuevo) {
        for (ServiciosAdicionales sa : servicios) {
            if (sa == null) {
                break;
            }
            if (nuevo.getCodigoServicio() == sa.getCodigoServicio()) {
                return true;
            }
        }
        return false;
    }

    public static Smartphone[] getSmartphones() {
        return smartphones;
    }

    public static void setSmartphones(Smartphone smartphone) {
        if (!existeSmartphone(smartphone)) {
            for (int i = 0; i < Cotizacion.smartphones.length; i++) {
                if (Cotizacion.smartphones[i] == null) {
                    Cotizacion.smartphones[i] = smartphone;
                    break;
                }
            }
        }
    }

    private static boolean existeSmartphone(Smartphone nuevo) {
        for (Smartphone phone : smartphones) {
            if (phone == null) {
                break;
            }
            if (nuevo.getId() == phone.getId()) {
                return true;
            }
        }
        return false;
    }

    public static void limpiar() {
        nombre = null;
        plan = null;
        servicios = new ServiciosAdicionales[99];
        smartphones = new Smartphone[99];
    }

    public static String getTable() {
        //creando tabla
        String content = "";
        String tr = "<tr>@@</tr>";
        String td = "<td>@@</td>";
        String msg = "";

        msg += td.replace("@@", 1 + "");
        msg += td.replace("@@", Cotizacion.getPlan().getNombrePlan() + "");
        msg += td.replace("@@", "$" + Cotizacion.getPlan().getPrecio());
        content += tr.replace("@@", msg);
        msg = "";
        int i = 2;
        int total = Cotizacion.getPlan().getPrecio();
        ServiciosAdicionales[] serviciosAdiconales = Cotizacion.getServicios();
        Smartphone[] phones = Cotizacion.getSmartphones();

        for (ServiciosAdicionales sa : serviciosAdiconales) {
            if (sa == null) {
                break;
            }
            msg += td.replace("@@", i + "");
            msg += td.replace("@@", sa.getNombreServicio());
            msg += td.replace("@@", "$10000");
            total += 10000;
            content += tr.replace("@@", msg);
            msg = "";
            i++;
        }

        for (Smartphone ph : Cotizacion.getSmartphones()) {
            if (ph == null) {
                break;
            }
            msg += td.replace("@@", i + "");
            msg += td.replace("@@", ph.getNombre() + "");
            msg += td.replace("@@", "$" + ph.getPrecio());
            total += ph.getPrecio();
            content += tr.replace("@@", msg);
            msg = "";
            i++;
        }

        msg += "<td colspan='2'>@@</td>".replace("@@", "Total");
        msg += td.replace("@@", "$" + total);
        content += tr.replace("@@", msg);
        return content;
    }

}
