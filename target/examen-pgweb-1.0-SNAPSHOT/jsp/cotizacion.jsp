<%@page import="com.mycompany.examen.pgweb.negocio.Cotizacion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cotizacion</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body class="vh-100 bg-light">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <header class="d-flex h-25 justify-content-between align-items-center container text-white bg-dark">
            <h1>Simulador de Planes</h1>
            <img class="h-75" src="./images/android.png" alt="">
        </header>
        <main class="container">
            <h3>
                Excelente <%=Cotizacion.getNombre()%>!! esta es tu cotización:
            </h3>
            <table class="table">
                <thead>
                    <tr>
                        <th colspan="3">Cotizacion</th>
                    </tr>
                    <tr>
                        <th>Item</th>
                        <th>Descripcion</th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <tbody>
                    <%=request.getAttribute("tablecontent")%>
                </tbody>
            </table>
            <a href="./index.html">
                <button class="btn btn-secondary">Crear otra cotizacion</button>
            </a>
        </main>
        
    </body>
</html>
