<%@page import="com.mycompany.examen.pgweb.negocio.Cotizacion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Planes</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body class="vh-100 bg-light">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <header class="d-flex h-25 justify-content-between align-items-center container text-white bg-dark">
            <h1>Simulador de Planes</h1>
            <img class="h-75" src="./images/android.png" alt="">
        </header>
        <main class="container">
            <h3>
                EL plan seleccionado es el <%=Cotizacion.getPlan().getNombrePlan()%> <br>
                Este plan cuenta con los siguientes servicios, <%=Cotizacion.getNombre()%> <br>
                ¿Deseas cotizar alguno?
            </h3>
            <form action="ContinueQuoting.do">
                <%=(String)request.getAttribute("services")%>
                <input class="btn btn-secondary" type="submit" value="Seguir cotizando">
            </form>
        </main>
        <footer class="h-25 d-flex justify-content-between pb-1 ps-1 pe-1 fixed-bottom">
            <div class="d-flex">
                <img src="./images/me.webp" alt="">
                <h4>Jeison Damian Murillo Sepulveda. cod 1151979 <br>
                    Cúcuta, Norte de Santander. 21/12/2021
                </h4>
            </div>
            <img src="./images/ufps.png" alt="">
        </footer>
    </body>
</html>
