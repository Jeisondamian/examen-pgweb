package com.mycompany.examen.pgweb.dto;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-12-23T09:46:49")
@StaticMetamodel(Smartphone.class)
public class Smartphone_ { 

    public static volatile SingularAttribute<Smartphone, String> marca;
    public static volatile SingularAttribute<Smartphone, Integer> precio;
    public static volatile SingularAttribute<Smartphone, Integer> id;
    public static volatile SingularAttribute<Smartphone, String> nombre;
    public static volatile SingularAttribute<Smartphone, String> urlImagen;

}